#include "Plane.h"


/*Constructor*/
/*Params : Vector2 normal, float distance*/
Plane::Plane(const glm::vec2& normal, float distance) : PhysicsObject(PLANE)
{
	/*Distance to the origin set to 0*/
	m_distanceToOrigin = distance;

	/*Normal vector2 set to (0,1)*/
	m_normal = normal;
}

/*Destructor*/
Plane::~Plane()
{
}

/*Fixed update*/
/*Params : Vector2 gravity, float timeStep*/
/*Because a plane is static the function will not contain a body*/
void Plane::fixedUpdate(glm::vec2 gravity, float timeStep)
{
}

/*makeGizmo*/
/*Makes the gizmo for a plane*/
void Plane::makeGizmo()
{
	/*The leghth of the line*/
	float lineSegmentLength = 300;

	/*Finds the centerpoint using normal * distance to origin*/
	glm::vec2 centerPoint = m_normal * m_distanceToOrigin;

	/*Parallel of the line created*/
	glm::vec2 parallel(m_normal.y, -m_normal.x);

	/*Vector 4 with colour code*/
	glm::vec4 colour(1, 1, 1, 1);

	/*The Start of the line which is calculated by centerpoint + parallel * the length whole line*/
	glm::vec2 start = centerPoint + (parallel * lineSegmentLength);

	/*The end of the line which is calculated by the centerpoint - parallel * the length whole line*/
	glm::vec2 end = centerPoint - (parallel * lineSegmentLength);

	/*Creates a gizmo of type 2Dline with a start, end, colour*/
	aie::Gizmos::add2DLine(start, end, colour);
}

void Plane::resetPosition()
{
}

/*Collision resolution for plane as it is not derived from rigidbody*/
void Plane::resolveCollision(Rigidbody* actor2, glm::vec2 contact)
{
	// the plane isn't moving, so the relative velocity is just actor2's velocity 
	glm::vec2 vRel = actor2->getVelocity();
	float e = actor2->getElasticity();
	float j = glm::dot(-(1 + e) * (vRel), m_normal) / (1 / actor2->getMass());

	glm::vec2 force = m_normal * j;

	actor2->applyForce(force, contact - actor2->getPosition());
}
