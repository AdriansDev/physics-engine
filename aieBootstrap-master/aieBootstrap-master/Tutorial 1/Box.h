#pragma once
#include "Rigidbody.h"
#include <glm\glm.hpp>
class Box : public Rigidbody

{
public:
	Box(glm::vec2 position, glm::vec2 extents, glm::vec2 velocity, float mass, glm::vec4 colour);
	~Box();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();

	bool checkBoxCorners(Box& box, glm::vec2& contact, int& numContacts, glm::vec2& edgeNormal, glm::vec2& contactForce);

	/*Getters*/
	glm::vec2 getExtents() { return m_extents; }
	glm::vec2 getLocalX() { return m_localX; }
	glm::vec2 getLocalY() { return m_localY; }
	glm::vec2 getCenter() { return m_Center; }

	/*Setter*/
	void setPosition(glm::vec2 position) { m_position = position; }

protected:
	/*Half edge lengths*/
	glm::vec2 m_extents;

	/*Colour of gizmo*/
	glm::vec4 m_colour;
	
	/*Stores the local x and y axes of the box based on its angle of rotation*/
	glm::vec2 m_localX;
	glm::vec2 m_localY;

	glm::vec2 m_Center;
};

