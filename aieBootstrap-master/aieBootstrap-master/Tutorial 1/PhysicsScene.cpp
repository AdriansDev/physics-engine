#include "PhysicsScene.h"
#include <algorithm>
#include <iostream>

/*Function pointer*/
typedef bool(*fn)(PhysicsObject*, PhysicsObject*);

/*Array of function pointers*/
static fn collisionFunctionArray[] =
{	PhysicsScene::plane2Plane,		PhysicsScene::plane2Sphere,		PhysicsScene::plane2box,
	PhysicsScene::Sphere2Plane,		PhysicsScene::sphere2Sphere,	PhysicsScene::sphere2box,
	PhysicsScene::box2Plane,		PhysicsScene::box2Sphere,		PhysicsScene::box2box};

/*Constructor*/
PhysicsScene::PhysicsScene()
{
	/*Timestep*/
	m_timeStep = 0.016f;

	/*Gravity*/
	m_gravity = glm::vec2(0, 0);
}

/*Deconstructor*/
PhysicsScene::~PhysicsScene()
{
	for (auto pActor : m_actors)
	{
		delete pActor;
	}
}

/*Add Actor*/
/*Params : physicsObject pointer called actor*/
/*Adds an actor to the scene*/
void PhysicsScene::addActor(PhysicsObject * actor)
{
	m_actors.push_back(actor);
}

/*Remove Actor*/
/*Params : physicsObject pointer called actor*/
/*Removes an actor from the scene*/
void PhysicsScene::removeActor(PhysicsObject * actor)
{
	remove(std::begin(m_actors), std::end(m_actors), actor);
}

/*Update*/
/*Params : Deltatime*/
/*Updates physics at a fixed timestep*/
void PhysicsScene::update(float dt)
{

	/*Float accumulated time*/
	static float accumulatedTime = 0.0f;
	accumulatedTime += dt;

	while (accumulatedTime >= m_timeStep)
	{
		/*Fixed update every loop*/
		for (auto pActor : m_actors)
		{
			pActor->fixedUpdate(m_gravity, m_timeStep);
		}
		accumulatedTime -= m_timeStep;
		checkForCollision();
	}
}

/*Updates Gizmos every frame*/
void PhysicsScene::updateGizmos()
{
	/*For every frame update gizmos*/
	for (auto pActor : m_actors)
	{
		pActor->makeGizmo();
	}
}

/*Debugs scene*/
void PhysicsScene::debugScene()
{
	/*Initial count*/
	int count = 0;

	/*Outputs the count and adds 1 onto the count every loop*/
	for (auto pActor : m_actors) {
		std::cout << count << " : ";
		pActor->debug();
		count++;
	}
}

/*Check for collision*/
/*Checks for collision of objects*/
void PhysicsScene::checkForCollision()
{
	/*actorCount = size of m_actors*/
	int actorCount = m_actors.size();

	for (int outer = 0; outer < actorCount - 1; outer++)
	{
		for (int inner = outer + 1; inner < actorCount; inner++)
		{
			/*Creates Physics objects*/
			PhysicsObject* object1 = m_actors[outer];
			PhysicsObject* object2 = m_actors[inner];

			/*Gets shape id from Physics object*/
			int shapeId1 = object1->getShapeID();
			int shapeId2 = object2->getShapeID();

			/*Using Function pointers*/
			int functionIdx = (shapeId1 * SHAPE_COUNT) + shapeId2;
			fn collisionFunctionPtr = collisionFunctionArray[functionIdx];
			if (collisionFunctionPtr != nullptr)
			{
				/*Did Collision Occur*/
				collisionFunctionPtr(object1, object2);
			}
		}
	}
}

bool PhysicsScene::Sphere2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	/*Creates the 2 objects*/
	Sphere* sphere = dynamic_cast<Sphere*>(obj1);
	Plane* plane = dynamic_cast<Plane*>(obj2);

	/*Collision detection*/
	if (sphere != nullptr && plane != nullptr)
	{
		glm::vec2 collisionNormal = plane->getNormal();
		float sphereToPlane = glm::dot(sphere->getPosition(), plane->getNormal()) - plane->getDistance();
		

		// if we are behind plane then we flip the normal
		if (sphereToPlane < 0)
		{
			collisionNormal *= -1;
			sphereToPlane *= -1;
		}

		float intersection = sphere->getRadius() - sphereToPlane;

		if (intersection > 0)
		{
			/*Collision resolution*/
			glm::vec2 contact = sphere->getPosition() + (collisionNormal * -sphere->getRadius());
			sphere->setPosition(sphere->getPosition() + plane->getNormal() * intersection);
			plane->resolveCollision(sphere, contact);
			return true;
		}
	}
	return false;
}

bool PhysicsScene::sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	/*Creates the 2 objects*/
	Sphere *sphere1 = dynamic_cast<Sphere*>(obj1);
	Sphere *sphere2 = dynamic_cast<Sphere*>(obj2);

	if (sphere1 != nullptr && sphere2 != nullptr)
	{

		if (sphere1 != nullptr && sphere2 != nullptr)
		{
			glm::vec2 delta = sphere2->getPosition() - sphere1->getPosition();
			float distance = glm::length(delta);
			float intersection = sphere1->getRadius() + sphere2->getRadius() - distance;

			glm::vec2 sphere1Pos = sphere1->getPosition();
			glm::vec2 sphere2Pos = sphere2->getPosition();

			float distanceX = sphere2Pos.x - sphere1Pos.x;
			float distanceY = sphere2Pos.y - sphere1Pos.y;

			if (intersection > 0)
			{
				glm::vec2 contactForce = 0.5f * (distance - (sphere1->getRadius() + sphere2->getRadius())) * delta / distance;

				sphere1->setPosition(sphere1->getPosition() + contactForce);
				sphere2->setPosition(sphere2->getPosition() - contactForce);

				sphere1->resolveCollision(sphere2, 0.5f * (sphere1->getPosition() + sphere2->getPosition()));

				return true;
			}
		}
		return false;
	}
}

bool PhysicsScene::plane2box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	box2Plane(obj2, obj1);
	return false;
}

bool PhysicsScene::sphere2box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	box2Sphere(obj2, obj1);
	return false;
}

bool PhysicsScene::box2box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	
	Box* box1 = dynamic_cast<Box*>(obj1);
	Box* box2 = dynamic_cast<Box*>(obj2);

	if (box1 != nullptr && box2 != nullptr)
	{
		glm::vec2 boxPos = box2->getPosition() - box1->getPosition();

		glm::vec2 norm(0, 0);
		glm::vec2 contactForce1, contactForce2;
		glm::vec2 contact(0, 0);
		
		int numContacts = 0;

		box1->checkBoxCorners(*box2, contact, numContacts, norm, contactForce1);

		if (box2->checkBoxCorners(*box1, contact, numContacts, norm, contactForce2))
		{
			norm = -norm;
		}
		if (numContacts > 0)
		{
			glm::vec2 contactForce = 0.5f*(contactForce1 - contactForce2);
			box1->setPosition(box1->getPosition() - contactForce);
			box2->setPosition(box2->getPosition() + contactForce);

			box1->resolveCollision(box2, contact / float(numContacts), &norm);
			return true;
		}
	}
	
	return false;
}

bool PhysicsScene::box2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Box* box = dynamic_cast<Box*>(obj1);
	Sphere* sphere = dynamic_cast<Sphere*>(obj2);

	if (box != nullptr && sphere != nullptr)
	{
		glm::vec2 circlePos = sphere->getPosition() - box->getPosition();
		float w2 = box->getExtents().x, h2 = box->getExtents().y ;

		int numContacts = 0;
		glm::vec2 contact(0, 0); // contact is in our box coordinates

		/* check the four corners to see if any of them are inside the circle*/
		for (float x = -w2; x <= w2; x += box->getExtents().x)
		{
			for (float y = -h2; y <= h2; y += box->getExtents().y)
			{
				glm::vec2 p = x * box->getLocalX() + y * box->getLocalY();
				glm::vec2 dp = p - circlePos;
				if (dp.x*dp.x + dp.y*dp.y < sphere->getRadius()*sphere->getRadius())
				{
					numContacts++;
					contact += glm::vec2(x, y);
				}
			}
		}
		/*Direction*/
		glm::vec2* direction = nullptr;
		/*Get the local position of the circle center*/
		glm::vec2 localPos(glm::dot(box->getLocalX(), circlePos), glm::dot(box->getLocalY(), circlePos));

		if (localPos.y < h2 && localPos.y > -h2)
		{
			if (localPos.x > 0 && localPos.x < w2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(w2, localPos.y);
				direction = new glm::vec2(box->getLocalX());
			}

			if (localPos.x < 0 && localPos.x > -(w2 + sphere->getRadius()))
			{
				numContacts++;
				contact += glm::vec2(-w2, localPos.y);
				direction = new glm::vec2(-box->getLocalX());
			}
		}
		if (localPos.x < w2 && localPos.x > -w2)
		{
			if (localPos.y > 0 && localPos.y < h2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(localPos.x, h2);
				direction = new glm::vec2(box->getLocalY());
			}
			if (localPos.y < 0 && localPos.y > -(h2 + sphere->getRadius())) 
			{
				numContacts++;
				contact += glm::vec2(localPos.x, -h2);
				direction = new glm::vec2(-box->getLocalY());
			}
		}
		if (numContacts > 0)
		{
			/*Average and convert back into world coords*/
			contact = box->getPosition() + (1.0f / numContacts)*(box->getLocalX()* contact.x + box->getLocalY()* contact.y);
			box->resolveCollision(sphere, contact, direction);
		}
		delete direction;
	}
	return false;
}

bool PhysicsScene::box2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	/*Creates box and plane*/
	Box* box = dynamic_cast<Box*>(obj1);
	Plane* plane = dynamic_cast<Plane*>(obj2);

	/*If we are successful then test for collision*/
	if (box != nullptr && plane != nullptr)
	{
		int numContacts = 0;
		glm::vec2 contact(0, 0);
		float contactV = 0;
		float radius = 0.5f * std::fminf(box->getExtents().x, box->getExtents().y);
		float penetration = 0;

		/*Which side is center of mass on*/
		glm::vec2 planeOrigin = plane->getNormal() * plane->getDistance();
		float comFromPlane = glm::dot(box->getPosition() - planeOrigin, plane->getNormal());

		/*Check all four coners to see if we have hit the plane*/
		for (float x = -box->getExtents().x; x <= box->getExtents().x; x += box->getExtents().x)
		{
			for (float y = -box->getExtents().y; y <= box->getExtents().x; y += box->getExtents().y)
			{
				/*get the position of the corner in world space*/
				glm::vec2 p = box->getPosition() + x * box->getLocalX() + y * box->getLocalY();

				float distFromPlane = glm::dot(p - planeOrigin, plane->getNormal());

				/*This is total velocity of the point*/
				float velocityIntoPlane = glm::dot(box->getVelocity() + box->getRotation() * (-y * box->getLocalX() + x * box->getLocalY()), plane->getNormal());

				/*if this corner is on the oppsite side from the COM, and moving further in, we need to resolve the collision*/

				if ((distFromPlane > 0 && comFromPlane < 0 && velocityIntoPlane > 0) || (distFromPlane < 0 && comFromPlane > 0 && velocityIntoPlane < 0))
				{
					numContacts++;
					contact += p;
					contactV += velocityIntoPlane;
				}
				if (comFromPlane >= 0) 
				{ 
					if (penetration > distFromPlane) penetration = distFromPlane; 
				}
				else 
				{ 
					if (penetration < distFromPlane) penetration = distFromPlane; 
				}
			}
		}
		/*We have had a hit - typicily 2 corners can contact*/
		if (numContacts > 0)
		{
			/*Get the average collision velocity into the plane*/
			/*(Covers linear and rotational velocity of all corners involved)*/
			float collisionV = contactV / (float)numContacts;

			/*get the acceleration required to stop (restitution = 0) or reverse*/
			/*(restitution = 1) the average velocity into the plane*/
			glm::vec2 acceleration = -plane->getNormal() * ((1.0f + box->getElasticity())* collisionV);

			/*and the average psition at which we'll apply the force (corner or edge center)*/
			glm::vec2 localContact = (contact / (float)numContacts) - box->getPosition();

			/*This is the perpendicular distance we apply the force at relative to the COM, so Torque = F*r*/
			float r = glm::dot(localContact, glm::vec2(plane->getNormal().y, -plane->getNormal().x));

			/* work out the "effective mass" - this is a combination of moment of inertia and mass, and tells us how much the contact point velocity*/
			/* will change with the force we're applying*/
			float mass0 = 1.0f / (1.0f / box->getMass() + (r*r) / box->getMoment());

			/*and apply the force*/
			box->applyForce(acceleration * mass0, localContact);

			box->setPosition(box->getPosition() - plane->getNormal() * penetration);
		}
	}
	return false;
}

bool PhysicsScene::plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}

bool PhysicsScene::plane2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Sphere2Plane(obj2, obj1);
	return false;
}

