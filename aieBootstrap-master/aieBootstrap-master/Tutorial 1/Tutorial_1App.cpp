#define _USE_MATH_DEFINES
#include "Tutorial_1App.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>
#include <cmath>
#include <imgui.h>
#include <gl_core_4_4.h>


Tutorial_1App::Tutorial_1App() {

}

Tutorial_1App::~Tutorial_1App() {

}

bool Tutorial_1App::startup() {

	//increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);
	
	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	/*Creates the physicsScene*/
	m_physicsScene = new PhysicsScene();
	m_physicsScene->setGravity(glm::vec2(0, -9.8));
	m_physicsScene->setTimeStep(0.01f);

	/*Creates the rocket and the kenetic curve*/
	/*Rocket = new Sphere(glm::vec2(0, 0), glm::vec2(0, 0), 300.0f, 1, glm::vec4(1, 0, 0, 1));
	radius = 0.5f;
	speed = 30.0f;
	startPos = glm::vec2(-40, 0);
	inclination = (float)M_PI / 4.0f; // 45 degrees
	m_physicsScene->addActor( new Sphere(startPos, inclination, speed, 1, radius, glm::vec4(1,0,0,1)));
	m_physicsScene->addActor(Rocket);*/


	Plane* m_planecol = new Plane(glm::vec2(0, 1), -56);
	m_physicsScene->addActor(m_planecol);

	Plane* m_planecol3 = new Plane(glm::vec2(-.707, 0.2), -75);
	m_physicsScene->addActor(m_planecol3);

	Plane* m_planecol4 = new Plane(glm::vec2(.707, 0.2), -75);
	m_physicsScene->addActor(m_planecol4);

	m_clearColour = glm::vec4(1, 1, 1, 1);

	return true;
}

void Tutorial_1App::shutdown() 
{
	delete m_physicsScene;
	delete m_font;
	delete m_2dRenderer;
}

void Tutorial_1App::setupContinuousDemo(glm::vec2 startPos, float inclination, float speed, float gravity)
{
	float t = 0;
	float tStep = 0.5f;
	float radius = 1.0f;
	int segments = 12;
	glm::vec4 colour = glm::vec4(1, 1, 0, 1);

	glm::vec2 m_position(startPos);
	glm::vec2 m_velocity(sin(inclination), cos(inclination));
	m_velocity *= speed;
	glm::vec2 force(0, gravity);

	while (t <= 5)
	{
		//Mass
		float mass = 1;

		glm::vec2 drawPos(0.0f, 0.0f);

       		drawPos.x = startPos.x + m_velocity.x * t;

		drawPos.y = startPos.y + m_velocity.y * t + (0.5* force.y * t * t);
		
		aie::Gizmos::add2DCircle(drawPos, radius, segments, colour);
		t += tStep;
	}

}

void Tutorial_1App::ShowImgui()
{


	ImGui::Begin("My Options");
	/*Box*/
	if (ImGui::CollapsingHeader("Box"))
	{
		if (ImGui::Button("Spawn Box"))
		{
			m_boxCol = new Box(glm::vec2(-50, 0), glm::vec2(2, 2), glm::vec2(30, 0), 1, glm::vec4(1, 1, 0, 1));
			m_physicsScene->addActor(m_boxCol);
		}
	}

	/*Sphere*/
	if (ImGui::CollapsingHeader("Sphere"))
	{
		if (ImGui::Button("Spawn Sphere"))
		{
			m_spherecol = new Sphere(glm::vec2(0, 10), glm::vec2(1, -10), 1, 2, glm::vec4(1, 1, 0, 1));
			m_physicsScene->addActor(m_spherecol);
		}
	}

	ImGui::End();
}

void Tutorial_1App::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	/*Rocket inputs*/
	/*When Space is pressed rocket will go up*/
	/*setupContinuousDemo(startPos, inclination, speed, m_physicsScene->getGravity().y);
 	if (input->isKeyDown(aie::INPUT_KEY_SPACE))
	{
		Sphere* exhaust = new Sphere(Rocket->getPosition(), Rocket->getVelocity(), 1, 1, glm::vec4(1, 1, 0, 1));
		//m_physicsScene->addActor(exhaust);
		float fuelConsumed = 0.0016f;

 		if (Rocket->getMass() > fuelConsumed)
		{
 			Rocket->m_mass -= fuelConsumed;
		}
		else
		{
			Rocket->m_mass = fuelConsumed;
		}

		Rocket->applyForce(glm::vec2(0.0f, 100.0f));
		exhaust->applyForce(glm::vec2(0.0f, -100.0f));
	}*/
	
	ShowImgui();
	

	
	m_physicsScene->updateGizmos();
	m_physicsScene->update(deltaTime);
	
	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial_1App::draw() {

	// wipe the screen to the background colour
	clearScreen();

	//glClearColor(m_clearColour.r, m_clearColour.g, m_clearColour.b, 1);

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));
	
	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
	aie::Gizmos::clear();
}