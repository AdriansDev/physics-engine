#include "Sphere.h"

/*Constructor*/
/*Params : Vector2 position, Vector2 velocity, float mass, float radius, Vector2 Colour*/
Sphere::Sphere(glm::vec2 position, glm::vec2 velocity, float mass, float radius, glm::vec4 colour) : Rigidbody(SPHERE, position, velocity, 0, mass)
	{
		/*Sets Radius*/
		m_radius = radius;

		/*Sets Colour*/
		m_colour = colour;

		/*Sets moment*/
		m_moment = 0.5f * mass * radius * radius;
	}

/*Constructor*/
/*Params : Vector2 position, Float inclination, float speed, float mass, float radius, Vector2 Colour*/
Sphere::Sphere(glm::vec2 position, float inclination, float speed, float mass, float radius, glm::vec4 colour) : 
	Sphere(position, glm::vec2(sin(inclination), cos(inclination))*speed, mass, radius, colour)
{
	
}

/*Deconstructor*/
Sphere::~Sphere()
{
}

/*makeGizmo*/
/*Makes the gizmo for a sphere*/
void Sphere::makeGizmo()
{
	/*Creates a gizmo of type 2DCircle with a position, radius , colour*/
	glm::vec2 end = glm::vec2(std::cos(m_rotation), std::sin(m_rotation)) * m_radius;

	aie::Gizmos::add2DCircle(m_position, m_radius, 12, m_colour);
	aie::Gizmos::add2DLine(m_position, m_position + end, glm::vec4(0, 0, 0, 1));
}

