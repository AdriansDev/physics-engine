#pragma once
#include "PhysicsObject.h"
#include <glm/glm.hpp>
#include <Gizmos.h>
#include "Rigidbody.h"

class Plane : public PhysicsObject
{
public:
	
	/*Constructor*/
	/*Params : Vector2 normal, float distance*/
	Plane(const glm::vec2& normal, float distance);

	/*Deconstructor*/
	~Plane();

	/*Fixed update*/
	/*Params : Vector2 gravity, float timeStep*/
	virtual void fixedUpdate(glm::vec2 gravity, float timeStep);

	/*Debug*/
#ifdef _DEBUG

	virtual void debug() {}

#endif // DEBUG


	/*makeGizmo*/
	/*To create the gizmo that will be colided with*/
	virtual void makeGizmo();
	
	/*resetPosition*/
	/*Resets position of the gizmo created*/
	virtual void resetPosition();

	void resolveCollision(Rigidbody * actor2, glm::vec2 contact);

	/*Gets the normal and returns the normal*/
	glm::vec2 getNormal() { return m_normal;}

	/*Gets the distance and returns the distance to the origin*/
	float getDistance() { return m_distanceToOrigin; }

protected:
	/*Normal Vector2*/
	glm::vec2 m_normal;

	/*Distance to the origin*/
	float m_distanceToOrigin;
	
};

