#pragma once
#include <glm\glm.hpp>
#include "PhysicsObject.h"
#include <Gizmos.h>
class Rigidbody : public PhysicsObject
{
public:

	Rigidbody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity, float rotation, float mass);
	~Rigidbody();

	virtual void fixedUpdate(glm::vec2 gravity, float timestep);
	virtual void debug();
	void applyForce(glm::vec2 force, glm::vec2 pos);
	void ForceToActor(Rigidbody* actor2, glm::vec2 force, glm::vec2 pos);

	/*Collision resolution*/
	void resolveCollision(Rigidbody* actor2, glm::vec2 contact, glm::vec2* collisionNormal = nullptr);
	
	/*Gets Position*/
	glm::vec2 getPosition() { return m_position; }

	/*Get Rotation*/
	float getRotation() { return m_rotation; }

	/*Get Velocity*/
	glm::vec2 getVelocity() { return m_velocity; }

	/*Get Mass*/
	float getMass() { return m_mass; }

	/*Get Elasticity*/
	float getElasticity() { return m_elasticity; }

	/*Get Moment*/
	float getMoment() { return m_moment; }

protected:
	/*Position*/
	glm::vec2 m_position;

	/*Velocity*/
	glm::vec2 m_velocity;
	
	/*linearDrag*/
	float m_linearDrag;

	/*angularDrag*/
	float m_angularDrag;

	/*Elasticity*/
	float m_elasticity;

	/*Rotation*/
	float m_rotation;

	/*Angular Velocity*/
	float m_angularVelocity;

	/*Momentem*/
	float m_moment;

public:
	/*Mass*/
	float m_mass;
};

