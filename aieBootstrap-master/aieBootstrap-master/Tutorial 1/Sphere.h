#pragma once
#include <glm\glm.hpp>
#include "PhysicsObject.h"
#include "Rigidbody.h"
#include <Gizmos.h>
#include <glm\glm.hpp>

class Sphere : public Rigidbody
{
public:

	/*Constructor*/
	/*Params : Vector2 position, Vector2 velocity, float mass, float radius, Vector2 Colour*/
	Sphere(glm::vec2 position, glm::vec2 velocity,
		float mass, float radius, glm::vec4 colour);

	/*Constructor*/
	/*Params : Vector2 position, Float inclination, float speed, float mass, float radius, Vector2 Colour*/
	Sphere(glm::vec2 position, float inclination, float speed,
		float mass, float radius, glm::vec4 colour);
	~Sphere();

	/*makeGizmo*/
	/*To create the gizmo that will be colided with*/
	virtual void makeGizmo();

	/*Gets Radius*/
	float getRadius() { return m_radius; }

	/*Gets Colour*/
	glm::vec4 getColour() { return m_colour; }

	/*Gets Position*/
	glm::vec2 getPosition() { return m_position; }

	/*Gets Velocity*/
	glm::vec2 getVelocity() { return m_velocity; }

	/*Sets Velocity*/
	void setVelocity(glm::vec2 velocity) { m_velocity = velocity; }

	/*Sets Position*/
	void setPosition(glm::vec2 position) { m_position = position; }

protected:

	/*Radius*/
	float m_radius;

	/*Colour*/
	glm::vec4 m_colour;

	

};

