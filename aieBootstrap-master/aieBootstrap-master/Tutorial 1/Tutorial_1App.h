#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include <glm\glm.hpp>
#include "Sphere.h"
#include "Rigidbody.h"
#include "PhysicsObject.h"
#include "Box.h"

class Tutorial_1App : public aie::Application {
public:

	Tutorial_1App();
	virtual ~Tutorial_1App();

	virtual bool startup();
	virtual void shutdown();
	virtual void setupContinuousDemo(glm::vec2 startPos, float inclination, float speed, float gravity);
	virtual void ShowImgui();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	glm::vec4 m_clearColour;
	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	PhysicsScene* m_physicsScene;
	Sphere* Rocket;
	float inclination;
	float radius;
	float speed;
	float topPlane;
	glm::vec2 startPos;

	Sphere* m_spherecol;
	Plane* m_planecol;
	Box* m_boxCol;
	
};