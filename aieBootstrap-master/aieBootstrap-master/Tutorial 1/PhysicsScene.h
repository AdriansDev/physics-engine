#pragma once
#include "PhysicsObject.h"
#include <glm\ext.hpp>
#include <vector>
#include "Rigidbody.h"
#include <list>
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"

class PhysicsScene
{
public:
	/*Constructor*/
	PhysicsScene();

	/*Deconstructor*/
	~PhysicsScene();

	/*Add Actor*/
	/*Params : PhysicsObject* of name actor*/
	/*Adds an actor to the scene*/
	void addActor(PhysicsObject* actor);

	/*Remove actor*/
	/*Params : PhysicsObject* of name actor*/
	/*Removes an actor from the scene*/
	void removeActor(PhysicsObject* actor);

	/*Update*/
	/*Params : Deltatime*/
	void update(float dt);

	/*UpdateGizmos*/
	/*Updates gizmos every frame*/
	void updateGizmos();

	/*Debug*/
#ifdef _DEBUG
	void debugScene();
#endif // DEBUG

	/*Check Collision*/
	/*Checks for collision*/
	void checkForCollision();

	/*Checks for PhysicsObjects to PhysicsObjects Collision*/
	static bool plane2Plane(PhysicsObject*, PhysicsObject*);
	static bool plane2Sphere(PhysicsObject*, PhysicsObject*);
	static bool Sphere2Plane(PhysicsObject*, PhysicsObject*);
	static bool sphere2Sphere(PhysicsObject*, PhysicsObject*);
	static bool plane2box(PhysicsObject*, PhysicsObject*);
	static bool sphere2box(PhysicsObject*, PhysicsObject*);
	static bool box2box(PhysicsObject*, PhysicsObject*);
	static bool box2Sphere(PhysicsObject*, PhysicsObject*);
	static bool box2Plane(PhysicsObject*, PhysicsObject*);
	
	/*Gets and Sets Gravity*/
	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; }
	glm::vec2 getGravity() const { return m_gravity; }

	/*Gets and Sets timeStep*/
	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float getTimeStep() const { return m_timeStep; }

protected:
	/*Vector 2 gravity*/
	glm::vec2 m_gravity;

	/*Float timeStep*/
	float m_timeStep;

	/*Vector cast to a PhysicsObject called m_actors*/
	std::vector<PhysicsObject*> m_actors;

};

